package ort.jav;

import java.util.Scanner;

public class TestPalindromOrNot {
	public static void main(String[] args) {
        int a = 121;
        int b = 125;
        
        checkPalindrome(a);
        checkPalindrome(b);
    }
    
    public static void checkPalindrome(int num)
    {
        int rem, rev = 0;
              
        for (int temp = num; temp > 0; temp /= 10)  
        {
           rem = temp % 10;                         
           rev = rev * 10 + rem;                    
        }
        if (rev == num)                             
        {
           System.out.println(num + " is a palindrme.");
        } 
        else
        {
           System.out.println(num + " is not a palndrome.");
        }
    }
}
