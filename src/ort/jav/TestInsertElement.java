package ort.jav;

public class TestInsertElement {
	public static void main(String[] args) {
        int[] array = {2, 9, 5, 0};
        int addnum = 4;

        System.out.print("Original array: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        
        
        int[] newArray = new int[array.length + 1];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        
        newArray[newArray.length - 1] = addnum;

        System.out.print("New array: ");
        for (int i = 0; i < newArray.length; i++) {
            System.out.print(newArray[i] + " ");
        }
       
    }
}

